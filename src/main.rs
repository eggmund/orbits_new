mod body;
mod tools;

use rayon::prelude::*;
use coffee::graphics::{Color, Frame, Window, WindowSettings, Point, Vector};
use coffee::load::Task;
use coffee::{Game, Result, Timer};
use std::collections::HashMap;
use std::sync::{Arc, RwLock};
use std::cell::RefCell;

use body::{Body, BodyVariant};

const SCREEN_DIMS: (u32, u32) = (1280, 720);

const TICKS_PER_SECOND: u16 = 60;
pub const DT_TICK: f32 = 1.0/TICKS_PER_SECOND as f32;

const G: f32 = 6.67e-2;


fn main() -> Result<()> {
    Orbits::run(WindowSettings {
        title: String::from("Orbits"),
        size: SCREEN_DIMS,
        resizable: false,
        fullscreen: false,
        maximized: false,
    })
}

struct Orbits {
    bodies: Arc<RwLock<HashMap<u64, Body>>>,
    body_id_counter: u64,
    assets: Assets,
}

impl Orbits {
    pub fn new() -> Self {
        let mut this = Orbits {
            bodies: Arc::new(RwLock::new(HashMap::new())),
            body_id_counter: 0,
            assets: Assets::default(),
        };

        this.add_body(Body::new(
            BodyVariant::Planet,
            Point::new(200.0, 200.0),
            Vector::new(50.0, 0.0),
            2.0,
            None,
        ));

        this.add_body(Body::new(
            BodyVariant::Planet,
            Point::new(400.0, 400.0),
            Vector::new(0.0, 0.0),
            10.0,
            None,
        ));

        this
    }

    pub fn add_body(&mut self, body: Body) {
        self.bodies.write().unwrap().insert(self.body_id_counter, body);
        self.body_id_counter = self.body_id_counter.wrapping_add(1);
    }

    pub fn force_between(b1: &Body, b2: &Body) -> Vector {
        // F = GMm/r^2
        // F_vec = GMm/r^2 * r_hat
        // r_hat = r_vec * 1/r
        // F_vec = GMm/r^3 * r_vec
        let r_vec = b2.pos - b1.pos;    // Force from 1 -> 2
        let r = r_vec.norm();
        r_vec * G * b1.mass * b2.mass/r.powi(3)
    }
}

impl Game for Orbits {
    type Input = (); // No input data
    type LoadingScreen = (); // No loading screen

    const TICKS_PER_SECOND: u16 = crate::TICKS_PER_SECOND;

    fn load(_window: &Window) -> Task<Orbits> {
        Task::succeed(|| Orbits::new())
    }

    fn update(&mut self, _window: &Window) {
        // Based on the fact that the bodies experience equal and opposite forces,
        // the number of updates can be reduced.

        let pairs = {
            let bodies_read = self.bodies.read().unwrap();
            let mut pairs: Vec<(u64, u64)> = Vec::with_capacity(bodies_read.len());
            // [1, 2, 3, 4] => [(1, 2), (1, 3), (1, 4), (2, 3), (2, 4), (3, 4)]
            {
                let keys: Vec<&u64> = bodies_read.keys().collect();
                for i in 0..keys.len()-1 {
                    for j in i+1..keys.len() {
                        pairs.push((*keys[i], *keys[j]));
                    }
                }
            }
            pairs
        };


        let bodies_share = Arc::clone(&self.bodies);
        pairs.par_iter().for_each(move |(id1, id2)| {
            let force = {
                let bodies_read = bodies_share.read().unwrap();
                Self::force_between(bodies_read.get(id1).unwrap(), bodies_read.get(id2).unwrap())
            };
            println!("Force between {} {}: {}", id1, id2, force);
            let mut bodies_write = bodies_share.write().unwrap();

            bodies_write.get_mut(id1).unwrap().net_force += force;
            bodies_write.get_mut(id2).unwrap().net_force -= force;
        });

        self.bodies.write().unwrap().par_iter_mut().for_each(move |(_, body)| {
            body.update();
        });
    }

    fn draw(&mut self, frame: &mut Frame, timer: &Timer) {
        // Clear the current frame
        frame.clear(Color::BLACK);
    
        let delta_factor: f32 = timer.next_tick_proximity();

        {
            let mut frame_target = frame.as_target();
            for (_, body) in self.bodies.read().unwrap().iter() {
                body.draw(&mut frame_target, delta_factor);
            }
        }
    }
}

#[derive(Default)]
struct Assets {
}

