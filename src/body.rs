use coffee::graphics::{Point, Vector, Shape, Color, Target};

use crate::DT_TICK;

#[derive(Clone)]
pub struct Body {
    pub pos: Point,
    vel: Vector,
    pub net_force: Vector,
    last_net_force: Vector,
    radius: f32,
    pub mass: f32,
    body_var: BodyVariant,
}

impl Body {
    pub fn new(body_var: BodyVariant, pos: Point, vel: Vector, radius: f32, mass: Option<f32>) -> Self {
        let mass = mass.unwrap_or_else(|| {
            let density = body_var.get_default_density();
            // m = d * v
            density * crate::tools::volume_of_sphere(radius)
        });

        Self {
            pos,
            vel,
            net_force: Vector::zeros(),
            last_net_force: Vector::zeros(),
            radius,
            mass,
            body_var,
        }
    }

    pub fn draw(&self, target: &mut Target, delta_factor: f32) {
        use coffee::graphics::{Mesh};

        let dt = DT_TICK * delta_factor;

        let lerp_vel = self.vel + self.net_force/self.mass * dt;

        let mut circ_mesh = Mesh::new();
        circ_mesh.fill(
            Shape::Circle {
                center: self.pos + lerp_vel * dt,
                radius: self.radius,
            },
            self.body_var.get_color(),
        );
        circ_mesh.draw(target);
    }

    pub fn update(&mut self) {
        self.vel += self.net_force/self.mass * DT_TICK;
        self.pos += self.vel * DT_TICK;
        self.last_net_force = self.net_force;
        self.net_force = Vector::zeros();
    }
}

#[derive(Clone)]
pub struct Light {
    luminosity: f32,
    pub color: Color,
}

#[derive(Clone)]
pub enum BodyVariant {
    Planet,
    Star {
        light: Light,
    },
}

impl BodyVariant {
    pub fn get_default_density(&self) -> f32 {
        match self {
            Self::Planet => 5000.0,
            Self::Star { .. } => 1400.0,
        }
    }

    pub fn get_color(&self) -> Color {
        match self {
            BodyVariant::Planet => Color::new(0.92, 0.9, 0.9, 1.0),
            BodyVariant::Star { light } => light.color,
        }
    }
}

pub fn get_light_from_temp(temperature: i64) -> Light {
    let color_rgb = colortemp::temp_to_rgb(temperature/2);  // Little bit of scaling
    println!("Color: {:?}", color_rgb);
    Light {
        luminosity: 1.0,
        color: Color::new(color_rgb.r as f32/255.0, color_rgb.g as f32/255.0, color_rgb.b as f32/255.0, 1.0),
    }
}
