
pub fn volume_of_sphere(r: f32) -> f32 {
    use std::f32::consts::PI;
    4.0/3.0 * PI * r.powi(3)
}

